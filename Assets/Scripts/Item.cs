﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

 public class Item : MonoBehaviour
{

    [SerializeField] private TMP_Text _textRus;
    [SerializeField] private TMP_Text _textEng;
    [SerializeField] private GameObject _translateObjedct;
    [SerializeField] private Button _translateButton;


    public Item() { }

    public void Init(string rus, string eng)
    {
        _textRus.text = rus;
        _textEng.text = eng;
    }
    public Item(string rus, string eng)
    {
        _textRus.text = rus;
        _textEng.text = eng;
    }

   
    public void SeeTranslate()
    {
        _translateButton.gameObject.SetActive(false);
        _translateObjedct.SetActive(true);
    }
    
}
public struct ItemDataInfo
{
    public string _textRus;
    public string _textEng;
}