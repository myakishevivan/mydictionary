﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public enum DictionaryType
    {
        HiddenRussian,
        HiddenEnglish
    }
    class DictionaryMode : BaseManager<DictionaryMode>, Imode
    {
        public DictionaryType DictionatyeType => UIController.Instance.GetDictionaryMode();
        public List<ItemDataInfo> DatasDicr { get; private set; }


        public  void Init()
        {
            DatasDicr = Data.Instance.DictionaryData;
            var window = WindowsController.Get<DictionaryWindow>();
            GetRandomItem();
            window.InitWindow();
            window.OnShow();
        }

        public  void StartGame()
        {
            throw new NotImplementedException();
        }

        public void GetRandomItem()
        {
            var rnd = new System.Random();
            for (int i = 0; i < DatasDicr.Count; i++)
            {
                var item = DatasDicr[i];
                DatasDicr.Remove(item);
                DatasDicr.Insert(rnd.Next(0, DatasDicr.Count), item);
            }
        }


    }
}
