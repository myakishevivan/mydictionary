﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnglishQuestsMode : BaseManager<EnglishQuestsMode>, Imode
{
    public List<EnglishQuest> englishQuests;
    private int index = -1;
    public  void Init()
    {
        EnglishQuestsWindow.OnNextQuestButtonClick += NextQuest;
        englishQuests = Data.Instance.EnglishQuestData;
        englishQuests.GetRandomList();
        var window = WindowsController.Get<EnglishQuestsWindow>();
        window.OnShow();
    }

    public  void StartGame()
    {
        throw new System.NotImplementedException();
    }

    private string NextQuest()
    {
        index++;

        if (index >= englishQuests.Count)
            index = 0;

        return englishQuests[index].Question;
    }
}
