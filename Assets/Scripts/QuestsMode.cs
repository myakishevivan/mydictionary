﻿using Assets.Scripts.windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public enum QuestsType
    {
        None,
        csharp,
        unity,
        git
    }

    class QuestsMode : BaseManager<QuestsMode>, Imode
    {
        public Dictionary<string, string> CurrentQuests { private get; set; }
        public QuestsType currnetQuestsType => UIController.Instance.GetQuestsMode();
        public bool IsRanomQuests { get; private set; }
        private int index = -1;
        private QuestsWindow window;
        private List<string> questskeys;
        private List<string> randomQuestskeys;
        public static event Action<string> onWarinigActive;

        private string questionsComplited;
        public QuestsMode()
        {
        }

        private void SetCurrnetData()
        {
            var data = Data.Instance.DevelopQuestsData;
            data.TryGetValue(currnetQuestsType, out Dictionary<string, string> currnetData);
            IsRanomQuests = UIController.Instance.randomQuestionsToggle.isOn; 

            if (currnetData != null)
                CurrentQuests = currnetData;
            else
                throw new ArgumentException("current data is null");



            questskeys = CurrentQuests.Keys.ToList();
            randomQuestskeys = new List<string>();
            randomQuestskeys.AddRange(questskeys);
            randomQuestskeys.GetRandomList();
            if (questskeys.Count == 0)
            {
                Debug.LogError("Keys count is 0");
            }
        }

        public string SetTheme()
        {
            return currnetQuestsType.ToString();
        }

        public void ChangeQuestion(out string key, out string value)
        {
            index++;

            if (index >= questskeys.Count)
                index = 0;

            if (IsRanomQuests)
                key = randomQuestskeys[index];
            else
                key = questskeys[index];

            value = CurrentQuests[key];
            SendCounter(index);
        }

        private void SendCounter(int currentValue)
        {
            currentValue++;
            string str = "";

            if (String.IsNullOrEmpty(questionsComplited))
            {
                if (currentValue <= CurrentQuests.Keys.Count)
                {
                    str = $"{currentValue}/{CurrentQuests.Keys.Count}";
                }

                if(currentValue == CurrentQuests.Keys.Count)
                    questionsComplited = "Вопросы закаончилсь. Список вопросов начался сначала!";
            }
            else
            {
                str = questionsComplited;
            }

            onWarinigActive?.Invoke(str);
        }

        public void Init()
        {
            SetCurrnetData();
            window = WindowsController.Get<QuestsWindow>();
            window.InitWindow();
            ResetWarning();
            window.OnShow();
        }

        private void ResetWarning()
        {
            index = -1;
            questionsComplited = null;
        }

        public void StartGame()
        {
            throw new NotImplementedException();
        }
    }
}
