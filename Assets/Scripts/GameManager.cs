﻿using Assets.Scripts.DownloadGoogleSheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class GameManager : MonoBehaviour
    {


        public static ModeType currentModeType { get; set; }
        private GooglSheetLoader loader;
        private Data _data;
        [Tooltip("Bg config")]
        [SerializeField] private Sprite portraitBg;
        [SerializeField] private Sprite albumBg;
        [SerializeField] private Image currentBg;
        [SerializeField] private UIController mainUI;
        [SerializeField] private WindowsController windowsController;

        private Dictionary<ModeType, Imode> _managersDict;



        private void Awake()
        {
            mainUI.playButton.onClick.AddListener(LoadMode);
            SetBg();
            CVSLoader.onEndLoad += InitManagers;
        }

        private void Start()
        {
            currentModeType = ModeType.None;
             _data = new Data();
            loader = GetComponent<GooglSheetLoader>();
            loader.DownloadDataTable();
        }
        private void InitManagers()
        {
            _managersDict = new Dictionary<ModeType, Imode>()
            {
               {ModeType.Dictionary,  DictionaryMode.Instance},
               {ModeType.DevelopQuestions, QuestsMode.Instance},
               {ModeType.EnglishQuestions,  EnglishQuestsMode.Instance}
            };
            mainUI.setModeMenuButton.interactable = true;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }





        private void LoadMode()
        {
            _managersDict.TryGetValue(currentModeType, out Imode result);

            if (result != null)
                result.Init();
            else
                throw new Exception("there is no manager with type " + currentModeType);
        }
        private void SetBg()
        {
#if UNITY_ANDROID
            currentBg.sprite = portraitBg;
#else
 currentBg.sprite = albumBg;
#endif

        }





    }
}
