﻿using Assets.Scripts.Save;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public enum ModeType
    {
        None,
        Dictionary,
        DevelopQuestions,
        EnglishQuestions
    }

    public interface Imode
    {
          void StartGame();

          void Init();
    }
}
