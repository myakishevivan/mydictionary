﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseManager<T> where T: class, new()
{
    private static T _instance;
    public static T Instance => _instance ?? (_instance = new T());

}
