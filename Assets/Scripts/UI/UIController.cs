﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static TMPro.TMP_Dropdown;

public class UIController : MonoBehaviour
{
    private static UIController _instance;
    public static UIController Instance => _instance;
    [Tooltip("Based menu")]
    [SerializeField] private GameObject BaseMenuContainer;
    [SerializeField] private GameObject DictionaryMenu;
    [SerializeField] private GameObject QuestMenu;
    [SerializeField] private GameObject EnglishQuestMenu;
    [SerializeField] private Button _setModeMenuButton;
    [SerializeField] private TMP_Dropdown _dropdown;

    [Tooltip("Dictionary menu")]
    [SerializeField] private Toggle _rusHidetoggle;
    [SerializeField] private Toggle _engHidetoggle;

    [Tooltip("DevelopQuests menu")]
    [SerializeField] private Toggle _unityToggle;
    [SerializeField] private Toggle _cSharpeToggle;
    [SerializeField] private Toggle _gitToggle;
    [SerializeField] private Toggle _randomQuestionsToggle;
    [Tooltip("Eng Quests menu")]
    [SerializeField] private TMP_Text _textMaxTime;

    [SerializeField] private Button _playButton;
    public TMP_Dropdown dropDown => _dropdown;
    public Button playButton => _playButton;
    public Button setModeMenuButton => _setModeMenuButton;
    public Toggle rusHidetoggle => _rusHidetoggle;
    public Toggle engHidetoggle => _engHidetoggle;
    public Toggle unityToggle => _unityToggle;
    public Toggle cSharpToggle => _cSharpeToggle;
    public Toggle gitToggle => _gitToggle;
    public Toggle randomQuestionsToggle => _randomQuestionsToggle;

    [SerializeField]
    private List<Toggle> toggles;
    private string time;

    public Dictionary<QuestsType, Toggle> questsToggles { get; private set; }

    private void Awake()
    {
        _instance = this;
    }
    private void Start()
    {
        _setModeMenuButton.interactable = false;
        playButton.gameObject.SetActive(false);
        QuestMenu.gameObject.SetActive(false);
        DictionaryMenu.gameObject.SetActive(false);
        EnglishQuestMenu.gameObject.SetActive(false);
        BaseMenuContainer.SetActive(true);
        foreach (var toggle in toggles)
        {
            toggle.onValueChanged.AddListener(delegate
            {
                OnToggleChange(toggle);
            });

        }
        questsToggles = new Dictionary<QuestsType, Toggle>()
        {

            { QuestsType.csharp, cSharpToggle },
            { QuestsType.unity, unityToggle },
            { QuestsType.git, gitToggle }
        };
      

        InstantiateDropDown();

        setModeMenuButton.onClick.AddListener(SetMenu);
        setModeMenuButton.onClick.AddListener(SetToggleInitialValie);
    }

    private void SetMaxSpeekTime( )
    {
        _textMaxTime.text = time;
    }

    private void SetMenu()
    {
        var selectedMode = (dropDown.options[dropDown.value] as OptionsDataGameType).ModeType;
        GameManager.currentModeType = selectedMode;
        BaseMenuContainer.gameObject.SetActive(false);
        playButton.gameObject.SetActive(true);
        switch (selectedMode)
        {
            case ModeType.Dictionary:
                DictionaryMenu.gameObject.SetActive(true);
                break;
            case ModeType.DevelopQuestions:
                QuestMenu.gameObject.SetActive(true);
                break;
            case ModeType.EnglishQuestions:
                EnglishQuestMenu.gameObject.SetActive(true);
                SetMaxSpeekTime();
                break;
            default:
                Debug.LogError("There is no ModeType " + selectedMode);
                break;

        }
    }

    private void InstantiateDropDown()
    {
        dropDown.ClearOptions();
        var optionsDataGamesTypes = new List<OptionData>();
        foreach (ModeType name in Enum.GetValues(typeof(ModeType)))
        {
            if (name == ModeType.None)
                continue;
            optionsDataGamesTypes.Add(new OptionsDataGameType(name));
        }
        dropDown.AddOptions(optionsDataGamesTypes);
    }


    public DictionaryType GetDictionaryMode()
    {
        DictionaryType type;
        if (rusHidetoggle.isOn)
            type = DictionaryType.HiddenRussian;
        else
            type = DictionaryType.HiddenEnglish;

        return type;
    }

    public QuestsType GetQuestsMode()
    {
        QuestsType type;
        type = questsToggles.Where(x => x.Value.isOn == true).Select(x => x.Key).FirstOrDefault();

        if (type == QuestsType.None)
            Debug.LogWarning("Quests type is NONE" + type);

        return type;
    }

    private void SetToggleInitialValie()
    {
        toggles.ForEach(x => x.isOn = false);
       

        if(rusHidetoggle.IsActive())
        rusHidetoggle.isOn = true;

        if(unityToggle.IsActive())
            unityToggle.isOn = true;

        if (randomQuestionsToggle.IsActive())
            randomQuestionsToggle.isOn = false;
    }

    private void OnToggleChange(Toggle toggle)
    {
        if (toggle.isOn)
        {
            toggles.Remove(toggle);
            foreach (var item in toggles)
            {
                if (item.IsActive())
                    item.isOn = false;
            }

            toggles.Add(toggle);
        }

    }
}
