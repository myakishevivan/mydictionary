﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
   public static class Helper
    {
        public static bool CheckString(this string value)
        {
            if (string.IsNullOrEmpty(value)) 
            {

                throw new ArgumentException($"string {value}  is IsNullOrEmpty ");
            }
            if (string.IsNullOrWhiteSpace(value))
            {

                throw new ArgumentException($"string {value}  is IsNullOrWhiteSpace ");
            }

            return true;
        }

        public static void GetRandomList<T>(this List<T> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                var currentValue = values[i];
                values.Remove(currentValue);
                var index = UnityEngine.Random.Range(0, values.Count);
                values.Insert(index, currentValue);
            }
        }
    }
}
