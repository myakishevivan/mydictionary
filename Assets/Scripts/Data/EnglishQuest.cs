﻿using System;
using UnityEngine;

public struct EnglishQuest
{
    public string Question { get; private set; }
    public TimeSpan MaxTalkTime { get; private set; }

    public EnglishQuest(string question)
    {
        MaxTalkTime = new TimeSpan();
        Question = question;
    }
}