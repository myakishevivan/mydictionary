﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Assets.Scripts.DownloadGoogleSheet;

public class Data
{
    private static Data _instance;
    public static Data Instance => _instance ?? (_instance = new Data());

    public List<ItemDataInfo> DictionaryData { get; private set; }
    public Dictionary<QuestsType, Dictionary<string, string>> DevelopQuestsData { get; private set; }
    public List<EnglishQuest> EnglishQuestData { get; private set; }
    public Data()
    {
        _instance = this;
        GooglSheetLoader.onProcessDataDictionary += SetDictionaryData;
        GooglSheetLoader.onProcessDataQuests += SetDevelopQuestsData;
        GooglSheetLoader.onProcessDataEnglishQuests += SetEnglishQuestData;
    }

    private void SetDictionaryData(List<ItemDataInfo> dictionaryData)
    {
        DictionaryData = dictionaryData;
    }
    private void SetDevelopQuestsData(Dictionary<QuestsType, Dictionary<string, string>> developQuestsData)
    {
        DevelopQuestsData = developQuestsData;
    }
    private void SetEnglishQuestData(List<EnglishQuest> englishQuests)
    {
        EnglishQuestData = englishQuests;
    }

    //public  T GetData<T>(ModeType modeType) where T : class
    //{
    //    T data;

    //    switch (modeType)
    //    {
    //        case ModeType.Dictionary:
    //            data = DictionaryData as T;
    //            break;
    //        case ModeType.EnglishQuestions:
    //            data = EnglishQuestData as T;
    //            break;
    //        case ModeType.DevelopQuestions:
    //            data = DevelopQuestsData as T;
    //            break;
    //        default:
    //            throw new Exception("Where is no modeType " + modeType);
    //    }

    //    return data ?? throw new ArgumentException($"{modeType} data  is null");
    //}

    
    
}
