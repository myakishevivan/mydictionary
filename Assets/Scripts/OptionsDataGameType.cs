﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TMPro.TMP_Dropdown;

namespace Assets.Scripts
{
    class OptionsDataGameType : OptionData
    {
        public ModeType ModeType { get; private set; }
        public OptionsDataGameType(ModeType modeType) : base(modeType.ToString())
        {
            ModeType = modeType;

        }
    }
}
