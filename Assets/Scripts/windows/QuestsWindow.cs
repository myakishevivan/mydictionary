﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Assets.Scripts.windows
{
    class QuestsWindow : BaseWindow
    {
        [SerializeField] private TMP_Text _themText;
        [SerializeField] private TMP_Text _headerQuest;
        [SerializeField] private TMP_Text _questText;
        [SerializeField] private Button _nextQuestButton;
        [SerializeField] private TMP_Text endingQuestText;

        public Dictionary<string, string> Quests { private get; set; }

     
        public override void OnShow()
        {
            _nextQuestButton.onClick.AddListener(ChangeQuestion);
            ChangeQuestion();
            SetTheme();
            base.OnShow();
        }
        private void SetTheme()
        {
            _themText.text = QuestsMode.Instance.SetTheme();
        }

        private void SetWarningActive(string str)
        {
            endingQuestText.text = str;
        }
        private void ChangeQuestion()
        {
            QuestsMode.Instance.ChangeQuestion(out string key, out string value);

            _questText.text = value;
            _headerQuest.text = key;

        }

        public override void InitWindow( )
        {
            QuestsMode.onWarinigActive += SetWarningActive;
        }


        public override void OnHide()
        {
            _nextQuestButton.onClick.RemoveAllListeners();
            base.OnHide();
        }





    }
}
