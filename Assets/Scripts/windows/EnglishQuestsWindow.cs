﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

enum ControllButtonMode
{
    Play,
    Stop
}

public class EnglishQuestsWindow : BaseWindow
{
    [SerializeField] private TMP_Text _textTime;
    [SerializeField] private TMP_Text _textQuestion;
    [SerializeField] private TMP_Text _textControllButton;
    [SerializeField] private Button _timerControllButton;
    [SerializeField] private Button _nextTextButton;

    public static event Action OnControllButtonClick; 
    public static event Func<string> OnNextQuestButtonClick; 
  
    private void OnEnable()
    {
        _timerControllButton.onClick.AddListener(ControllButtonClick);
        _nextTextButton.onClick.AddListener(NextQuestButtonClick);
    }
    public override void OnShow()
    {
        NextQuestButtonClick();
        base.OnShow();
    }
    private void NextQuestButtonClick()
    {
        var questText = OnNextQuestButtonClick?.Invoke();
        _textQuestion.text = questText;
    }

    private void ControllButtonClick()
    {
        OnControllButtonClick?.Invoke();
    }

    private void OnDisable()
    {
        _timerControllButton.onClick.RemoveAllListeners();
        _nextTextButton.onClick.RemoveAllListeners();
    }
}
