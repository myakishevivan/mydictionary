﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WindowsController : MonoBehaviour
{
    private static WindowsController instance_;
    private Dictionary<Type, BaseWindow> allWindows;
    private Dictionary<Type, BaseWindow> _windowsTypes;
    public List<BaseWindow> _showingWindows { get; private set; }
    public static WindowsController Instance
    {
        get
        {
            return instance_;
        }
    }

    private void Awake()
    {
        instance_ = this;
        BaseWindow[] arrayWindows = GetComponentsInChildren<BaseWindow>(true);
        InActiveWindows(arrayWindows);
        _showingWindows = new List<BaseWindow>();
        allWindows = new Dictionary<Type, BaseWindow>();
        _windowsTypes = new Dictionary<Type, BaseWindow>();

        for (int i = 0; i < arrayWindows.Length; i++)
        {
            var curWindow = arrayWindows[i];
            var windowType = curWindow.GetType();

            if (!allWindows.ContainsKey(windowType))
            {
                allWindows.Add(windowType, curWindow);
                _windowsTypes.Add(windowType, curWindow);
            }
            else
            {
                Debug.LogError("Double window " + curWindow.GetType().ToString());

            }
        }

    }

    private void InActiveWindows(BaseWindow[] arrayWindows)
    {
        arrayWindows.ToList().ForEach(x => x.gameObject.SetActive(false));
    }

    public static void Show(BaseWindow window)
    {
        var windowType = window.GetType();
        OnShow(windowType);
    }

    private static void OnShow(Type windowType)
    {
        if (instance_ != null)
        {
            if (instance_.allWindows.TryGetValue(windowType, out BaseWindow window))
            {
                window.OnShow();
                instance_._showingWindows.Add(window);
            }
        }
        else
        {
            throw new ArgumentException("Dont show window " + windowType);
        }
    }

    private static void HideWindow(BaseWindow window)
    {
        OnHide(window.GetType());

    }

    public static void OnHide(Type type)
    {
        if (instance_ != null)
        {
            if (instance_.allWindows.TryGetValue(type, out BaseWindow window))
            {
                if (instance_._showingWindows.Contains(window))
                {
                    window.OnHide();
                    instance_._showingWindows.Remove(window);
                }
                window.OnShow();
            }
        }
    }

    public static T Get<T>() where T : BaseWindow
    {
        if (instance_ == null)
            throw new ArgumentException("instance_ is null");

        var windowType = typeof(T);

        if (instance_.allWindows.TryGetValue(windowType, out BaseWindow window))
        {
            return window as T;

        }
        else
        {
            throw new ArgumentException("there is no window with type: " + windowType);
        }
    }
}
