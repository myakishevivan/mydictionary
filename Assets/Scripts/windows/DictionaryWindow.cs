﻿using Assets.Scripts;
using Assets.Scripts.DownloadGoogleSheet;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class DictionaryWindow : BaseWindow
{
    [SerializeField] private Item itemObj;
    [SerializeField] private RectTransform _content;
    [SerializeField] private Scrollbar _scroll;
    [SerializeField] private TMP_Text _textCount;
    private List<Item> installedItems;
    public override void InitWindow()
    {
        
        installedItems = new List<Item>();
        itemObj.gameObject.SetActive(true);
        OnShow();
        InstanitiateDictionary();
    }

   

    public void InstanitiateDictionary()
    {
        var randomItemsInfo = DictionaryMode.Instance.DatasDicr;
        var currentGameMode = DictionaryMode.Instance.DictionatyeType;

        for (int i = 0; i < randomItemsInfo.Count; i++)
        {
            var itemInfo = randomItemsInfo[i];

            var item = Instantiate(itemObj, _content);

            if (currentGameMode == DictionaryType.HiddenRussian)
                item.Init(itemInfo._textRus, itemInfo._textEng);
            else
                item.Init(itemInfo._textEng, itemInfo._textRus);

            installedItems.Add(item);
        }

        StartCoroutine(WaitAndUpScrollBar());
        _textCount.text = randomItemsInfo.Count.ToString();
    }

   
    private IEnumerator WaitAndUpScrollBar()
    {
        yield return null;
        _scroll.value = 1;
        itemObj.gameObject.SetActive(false);
    }

    public override void OnHide()
    {
        foreach (var item in installedItems)
        {
            Destroy(item.gameObject);
        }
        base.OnHide();
    }

}
