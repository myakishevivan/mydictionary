﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BaseWindow : MonoBehaviour
{
    [HideInInspector]
    public UnityEvent OnHideEndEvent = new UnityEvent();
    [HideInInspector]
    public UnityEvent OnShowEndEvent = new UnityEvent();
    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }
    virtual public void InitWindow( )
    {

    }
    protected virtual void OnShowEnd()
    {
        OnShowEndEvent.Invoke();
        OnShowEndEvent.RemoveAllListeners();
    }
    public virtual void OnShow()
    {
        if (WindowsController.Instance._showingWindows.Contains(this))
            return;
        gameObject.SetActive(true);
    }
      public virtual void OnShow(BaseWindow window)
    {
        if (WindowsController.Instance._showingWindows.Contains(window))
            return;

        gameObject.SetActive(true);
    }

    public virtual void OnHide()
    {
        gameObject.SetActive(false);
    }

}
