﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class WarningWindow : BaseWindow
{
    [SerializeField] private TMP_Text textWarning;


   public void SetTextWarning(string text)
    {
        textWarning.text = text;
    }
    public override void OnHide()
    {
    }
}
