﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Save
{
    public abstract class SerializableManager<T> : SerializableManager where T : class, new()
    {
        private static T _instance;
        public static T Instance => _instance ?? (_instance = new T());

    }
        public abstract class SerializableManager
        {

        }
}
