﻿using Assets.Scripts.DownloadGoogleSheet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    class CVSLoader : MonoBehaviour
    {
        private static string url = "https://docs.google.com/spreadsheets/d/*/export?format=csv";
        private static string urlIdPages = "&gid=";
        private List<string> urls = new List<string>();

        public static event Action onEndLoad;
        public void DownoloadTable(Action<string> onSheetDictLoadedAction, Action<string> onSheetQuestsLoadedAction, Action<string> onSheetEnglishQuestsLoadedAction)
        {
            string urlforDict = url.Replace("*", GooglSheetLoader.Instance.SheetId); //заменяем звездочку на текущий id страницы id: 1Ku6aRhEB1iLlSsvsZw7KadYWmUkbTxRl2AB1i3GYJx8
            StartCoroutine(DownloadRawCvsTable(urlforDict, onSheetDictLoadedAction));
            urls.Add(urlforDict);
            string urlforQuests = urlforDict + urlIdPages + GooglSheetLoader.Instance.QuestsSheetId;
            StartCoroutine(DownloadRawCvsTable(urlforQuests, onSheetQuestsLoadedAction));
            urls.Add(urlforQuests);

             string urlforEnglishQuests = urlforDict + urlIdPages + GooglSheetLoader.Instance.QuestsEnglishSheetId;
            StartCoroutine(DownloadRawCvsTable(urlforEnglishQuests, onSheetEnglishQuestsLoadedAction));
            urls.Add(urlforEnglishQuests);


        }

        private IEnumerator DownloadRawCvsTable(string actualUrl, Action<string> callback)
        {
            using (UnityWebRequest request = UnityWebRequest.Get(actualUrl))
            {
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    var exeption = $"Can't load the file. May be intenet Connection is broken or invalid sheet link\n Exeption: {request.error} ";
                    var window = WindowsController.Get<WarningWindow>();
                    window.SetTextWarning(exeption);
                    window.OnShow();
                    Debug.LogError(request.error);
                }
                else
                {
                    Debug.Log("Succesful download");
                    Debug.Log(request.downloadHandler.text);
                }

                callback(request.downloadHandler.text);
                urls.Remove(actualUrl);
                if (urls.Count == 0)
                    onEndLoad?.Invoke();

            }

            yield return null;

        }
    }
}
