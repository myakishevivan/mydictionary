﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DownloadGoogleSheet
{
    /// <summary>
    /// Получаем сырой текст и обработаываем его для инициализации данных
    /// </summary>
    public class SheetProcessor : MonoBehaviour
    {
        //разделенеие клеток, разделение внутри клеток
        private const char CELL_SEPARATOR = ',';
        private const char IN_CELL_SEPARATOR = ';';
        public List<ItemDataInfo> ProcessDataDict(string rawCVSText)
        {
            char lineEnding = GetLineEnding();
            string[] rows = rawCVSText.Split(lineEnding);
            int dataStartRawIndex = 0;
            var ItemsDataInfo = new List<ItemDataInfo>();

            for (int i = dataStartRawIndex; i < rows.Length; i++)

            {
                string[] cell = rows[i].Split(CELL_SEPARATOR);
                var key = "";
                var value = "";
                try
                {
                    key = cell[0];
                    value = cell[1];
                }
                catch (Exception ex)
                {
                    Debug.LogError($"key " + key);
                    Debug.LogError($"value " + value);
                }


                if (key.CheckString() && value.CheckString())
                {
                    ItemsDataInfo.Add(new ItemDataInfo()
                    {
                        _textEng = key,
                        _textRus = value
                    });
                }
            }

            Debug.Log(ItemsDataInfo.ToString());
            return ItemsDataInfo;
        }

        public Dictionary<QuestsType, Dictionary<string, string>> ProcessDataQuests(string rawCVSText)
        {
            var ItemsDataInfo = new Dictionary<QuestsType, Dictionary<string, string>>();

            var cSharpDataInfo = new Dictionary<string, string>();
            var unityDataInfo = new Dictionary<string, string>();
            var gitDataInfo = new Dictionary<string, string>();

            char lineEnding = GetLineEnding();
            string[] rows = rawCVSText.Split(lineEnding);
            int dataStartRawIndex = 0;


            for (int i = dataStartRawIndex; i < rows.Length; i++)
            {
                string[] cell = rows[i].Split(CELL_SEPARATOR);
                int j = 0;
                var key = "";
                var value = "";
                var dictKey = "";
                try
                {
                    key = cell[j++];
                    value = cell[j++].Replace(IN_CELL_SEPARATOR, '\n').Replace(' ', '\u00A0');
                    dictKey = cell[j++].TrimEnd();
                }
                catch (Exception ex)
                {
                    Debug.LogError($"{ex.Message} key " + key );
                    Debug.LogError($"{ex.Message} value " + value);
                    Debug.LogError($"{ex.Message} dictKey " + dictKey);
                }

                if (key.CheckString() && value.CheckString() && dictKey.CheckString() && Enum.TryParse(dictKey,true, out QuestsType result))
                {

                    switch (result)
                    {
                        case QuestsType.csharp:
                            cSharpDataInfo.Add(key, value);
                            break;
                        case QuestsType.unity:
                            unityDataInfo.Add(key, value);
                            break;
                        case QuestsType.git:
                            gitDataInfo.Add(key, value);
                            break;
                        default:
                            Debug.LogError("there is no QuestsType " + dictKey);
                            break;
                    }
                }


            }

            ItemsDataInfo.Add(QuestsType.csharp, cSharpDataInfo);
            ItemsDataInfo.Add(QuestsType.unity, unityDataInfo);
            ItemsDataInfo.Add(QuestsType.git, gitDataInfo);
            return ItemsDataInfo;
        }

        public List<EnglishQuest> ProcessDataEnglishQuests(string rawCVSText)
        {
            List<EnglishQuest> engQuests = new List<EnglishQuest>();
            char lineEnding = GetLineEnding();
            string[] rows = rawCVSText.Split(lineEnding);
            int dataStartRawIndex = 0;

            for (int i = dataStartRawIndex; i < rows.Length; i++)
            {
                string value = "";

                try
                {
                    value = rows[i];
                }
                catch(Exception ex)
                {
                    Debug.LogError($"{ex.Message} key " + value);

                }

                if (value.CheckString())
                {
                    var engQuest = new EnglishQuest(value);
                    engQuests.Add(engQuest);
                }
            }
            return engQuests;
        }

        private char GetLineEnding()
        {
            char lineEnding = '\n';
#if UNITY_IOS
lineEnding = '\r';
#endif
            return lineEnding;
        }
    }
}