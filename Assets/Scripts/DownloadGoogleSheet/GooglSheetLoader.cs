﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace Assets.Scripts.DownloadGoogleSheet
{
    class GooglSheetLoader : MonoBehaviour
    {
        private static GooglSheetLoader _instance;
        public static GooglSheetLoader Instance => _instance ?? (_instance = new GooglSheetLoader());
        public static event Action<List<ItemDataInfo>> onProcessDataDictionary;
        public static event Action<Dictionary<QuestsType, Dictionary<string, string>>> onProcessDataQuests;
        public static event Action<List<EnglishQuest>> onProcessDataEnglishQuests;
        public string SheetId;
        public string QuestsSheetId;
        public string QuestsEnglishSheetId;
        public List<ItemDataInfo> _datasDicr { get; private set; }
        public Dictionary<string, Dictionary<string, string>> _questsData { get; private set; }

        private CVSLoader _cvsLoader;
        private SheetProcessor _sheetProcessor;

        void Awake()
        {
            _instance = this;
            _cvsLoader = GetComponent<CVSLoader>();
            _sheetProcessor = GetComponent<SheetProcessor>();


        }
        public void DownloadDataTable()
        {
            _cvsLoader.DownoloadTable(OnRawCvsLoadedDIct, OnRawCvsLoadedQuests, OnRawCvsLoadedEnglishQuests);
        }

        private void OnRawCvsLoadedDIct(string rawCVSText)
        {
            onProcessDataDictionary?.Invoke(_sheetProcessor.ProcessDataDict(rawCVSText));
        }
        private void OnRawCvsLoadedQuests(string rawCVSText)
        {
            onProcessDataQuests?.Invoke(_sheetProcessor.ProcessDataQuests(rawCVSText));
        }
        private void OnRawCvsLoadedEnglishQuests(string rawCVSText)
        {
            onProcessDataEnglishQuests?.Invoke(_sheetProcessor.ProcessDataEnglishQuests(rawCVSText));
        }

    }
}
